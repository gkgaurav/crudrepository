<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Visitor;
use App\Repositories\VisitorRepository;

class VisitorController extends Controller
{
    
 protected $VisitorRepository;

    public function __construct(VisitorRepository $VisitorRepository) {
        $this->VisitorRepository = $VisitorRepository;
   
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $visitor = $this->VisitorRepository->index();
        return view('backend.visitor.index',compact('visitor'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.visitor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, 
            [ 'name' => 'required',
            'email' => 'required',
            'mobile' => 'required',
            

        ]);

        $this->VisitorRepository->store($request);
        return redirect()->route('visitor.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $visitor = $this->VisitorRepository->find($id);
        return view('backend.visitor.show',compact('visitor'));

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $visitor = $this->VisitorRepository->find($id);
        return view('backend.visitor.edit',compact('visitor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->VisitorRepository->update($request,$id);
        return redirect()->route('visitor.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->VisitorRepository->delete($id);
        return redirect()->route('visitor.index');
    }
}
