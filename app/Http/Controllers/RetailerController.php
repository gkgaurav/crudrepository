<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Retailer;
use App\Repositories\RetailerRepository;
class RetailerController extends Controller
{
    


 protected $RetailerRepository;

    public function __construct(RetailerRepository $RetailerRepository) {
        $this->RetailerRepository = $RetailerRepository;
   
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $retailer = $this->RetailerRepository->index();
        return view('backend.retailer.index',compact('retailer'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('backend.retailer.create',compact('retailer'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


       $this->validate($request, 
            ['name' => 'required',
              'username' => 'required',
              
              'password' => 'required'

        ]);

        $this->RetailerRepository->store($request);

        return redirect()->route('retailer.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $retailer = $this->RetailerRepository->find($id);
        return view('backend.retailer.show',compact('retailer'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $retailer = $this->RetailerRepository->find($id);
        return view('backend.retailer.edit',compact('retailer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->RetailerRepository->update($request,$id);
      return redirect()->route('retailer.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->RetailerRepository->delete($id);
        return redirect()->route('retailer.index');
    }
}
