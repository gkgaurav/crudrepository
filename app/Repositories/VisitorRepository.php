<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Model\Visitor;
use App\Repositories\VisitorRepository;
use Illuminate\Support\Facades\Input;
use App\Helper\Common;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class VisitorRepository extends BaseRepository {

    /**
     * Specify Model class name
     *
     * @return string
     */
   public function model() {
        return "App\Model\Visitor";
    }

    public function index()
    {
      $visitor = Visitor::all();
      return $visitor;
    }

    public function store($request)
    {
      $visitor = $request->all();
      Visitor::create($visitor);
      return true;
    }

    public function edit($request,$id)
    {
      $visitor = Visitor::all($id);
      return $visitor;
    }
public function update($request,$id)
   {
   
   $visitor = Visitor::find($id);
   $data = $request->all();
   $visitor->update($data);
   return $visitor;
   

   }

}