<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Model\User;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Input;
use App\Helper\Common;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserRepository extends BaseRepository {

    /**
     * Specify Model class name
     *
     * @return string
     */
   public function model() {
        return "App\Model\User";
    }

    public function index()
    {
    	$user = User::all();
    	return $user;


    }

    public function store($request)
    {



         $user = $request->all();

         User::create($user);
         return true;

    }
   
   public function edit($request,$id)
   {
   	$user = User::all($id);
   	return $user;
   }

   public function update($request,$id)
   {
   $user = User::find($id);
   $data = $request->all();
   $user->update($data);
   
   return true;

   }


}