<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Model\Retailer;
use App\Repositories\RetailerRepository;
use Illuminate\Support\Facades\Input;
use App\Helper\Common;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class RetailerRepository extends BaseRepository {

    /**
     * Specify Model class name
     *
     * @return string
     */
   public function model() {
        return "App\Model\Retailer";
    }

    public function index()
    {
      $retailer = Retailer::all();
      return $retailer;


    }

    public function store($request)
    {

    $retailer = $request->all();
    Retailer::create($retailer);
    return true;

        

    }
   
   public function edit($request,$id)
   {

    $retailer = Retailer::all($id);
    return $retailer;
    
   }

   public function update($request,$id)
   {
   
   $retailer = Retailer::find($id);
   $data = $request->all();
   $retailer->update($data);
   return $retailer;
   

   }


}