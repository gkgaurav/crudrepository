<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Model\Customer;
use App\Repositories\CustomerRepository;
use Illuminate\Support\Facades\Input;
use App\Helper\Common;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class CustomerRepository extends BaseRepository {

    /**
     * Specify Model class name
     *
     * @return string
     */
   public function model() {
        return "App\Model\Customer";
    }

    public function index()
    {

      $customer = Customer::all();
      return $customer;
    }

    public function store($request)
    {
      $customer = $request->all();
      Customer::create($customer);
      return true;
    }

    public function edit($request, $id)
    {
      $customer = Customer::all($id);
      return $customer;
    }

    public function update($request,$id)
   {
   
   $customer = Customer::find($id);
   $data = $request->all();
   $customer->update($data);
   return $customer;
   

   }
   

}