<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
   protected $fillable = [
   'name', 'username', 'email', 'mobile', 'image'

   ];
}
