<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Retailer extends Model
{
    protected $fillable = [
    	'name', 'email', 'image', 'username', 'password'
    ];
}
