<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="{{(App\Helper\Common::getCurrentController() == 'AdminController') ? 'active' : ''}}"><a href="{{route('index.index')}}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
            <li class="{{(App\Helper\Common::getCurrentController() == 'CompanyController') ? 'active' : ''}}"><a href="{{route('user.index')}}"><i class="fa fa-bars"></i> <span>Users</span></a></li>

            <li class="{{(App\Helper\Common::getCurrentController() == 'CompanyController') ? 'active' : ''}}"><a href="{{route('retailer.index')}}"><i class="fa fa-bars"></i> <span>Retailers</span></a></li>

            <li class="{{(App\Helper\Common::getCurrentController() == 'CompanyController') ? 'active' : ''}}"><a href="{{route('customer.index')}}"><i class="fa fa-bars"></i> <span>Customers</span></a></li>


            <li class="{{(App\Helper\Common::getCurrentController() == 'CompanyController') ? 'active' : ''}}"><a href="{{route('visitor.index')}}"><i class="fa fa-bars"></i> <span>Visitor</span></a></li>

            <li class="treeview">
                <!--          <a href="#">
                            <i class="fa fa-dashboard"></i> <span>Users</span>
                            <span class="pull-right-container">
                              <i class="fa fa-angle-left pull-right"></i>
                            </span>
                          </a>-->
                <ul class="treeview-menu">
                    {{-- <li class="active"><a href="index.html"><i class="fa fa-circle-o"></i>Admins</a></li> --}}

                </ul>
            </li>
            
            
            <li class="{{(Route::currentRouteName() == 'backend.password')? 'active' : '' }}"><a href=""><i class="fa fa-cog"></i><span>Change Password</span></a></li>
            <li class="{{(Route::currentRouteName() == 'settings.create')? 'active' : '' }}"><a href=""><i class="fa fa-cog"></i><span>Settings</span></a></li>

            <li class="{{(Route::currentRouteName() == 'countries.create')? 'active' : '' }}"><a href=""><i class="fa fa-cog"></i><span>Countries</span></a></li>
              
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>